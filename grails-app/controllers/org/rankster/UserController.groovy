package org.rankster


import org.springframework.security.access.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
class UserController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond User.list(params), model:[userInstanceCount: User.count()]
    }

    def show(User userInstance) {
        respond userInstance
    }

    def create() {
        respond new User(params)
    }

    def register() {
        respond new User(params)
    }

    def myProfile(){
        def currentUser = springSecurityService.getCurrentUser()
        if(currentUser == null){
            redirect uri: "/"
        }else{
            def userInstance =  User.findById(currentUser.id) 
            respond userInstance 
        }
    }

    @Transactional
    def registerNewUser(User userInstance) {
        saveUser(userInstance)
    }

    @Transactional
    def save(User userInstance) {
        saveUser(userInstance)
        render view: view, model: [postUrl: postUrl,
                                   rememberMeParameter: config.rememberMe.parameter]
    }

    def edit(User userInstance) {
        respond userInstance
    }

    @Transactional
    def update(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'edit'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }

        userInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    private void saveUser(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }
        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:backTo
            return
        }
        userInstance.save flush:true
        redirect uri: '/'
    }

	@Transactional
	def uploadAvatar() {
		// captura o usuario atual
        def currentUser = springSecurityService.getCurrentUser()
        def userInstance =  User.findById(currentUser.id) 
		// cria o diretorio dos avatares
		String avatarDir = getAvatarDir(userInstance.id)

		// pega a image e salva 
		def f = request.getFile('avatarImage')
		if (f.empty) {
			flash.message = 'file cannot be empty'
			return
		}   
		String file_name = f.getOriginalFilename()
		String file_path = avatarDir+"/"+file_name
		userInstance.avatar = "user/"+userInstance.id+"/"+file_name 
        userInstance.save()
		// cria o arquivo
		f.transferTo(new File(file_path))
        redirect uri: "/user/myProfile"
	}

	def getAvatarDir(Long id){
		def webrootDir = servletContext.getRealPath("/") // app directory path
		String path = webrootDir+"images/user/"+id
		def directory = new File(path)	
		if(!directory.exists()){
			directory.mkdirs()
		}
		return path
	}
}
