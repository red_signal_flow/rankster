package org.rankster



import org.springframework.security.access.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured(['IS_AUTHENTICATED_FULLY'])
class ParticipationController {

    static allowedMethods = [save: "POST", upVote:"GET", update: "PUT", delete: "DELETE"]

    @Transactional
    def upVote(Participation p) {
        p.votes += 1
        p.save flush: true
        redirect controller: "rank", action:"show", id: p.rank.id, method:"GET"
    }
    @Transactional
    def downVote(Participation p) {
        p.votes -= 1
        p.save flush: true
        redirect controller: "rank", action:"show", id: p.rank.id, method:"GET"
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Participation.list(params), model:[participationInstanceCount: Participation.count()]
    }

    def show(Participation participationInstance) {
        respond participationInstance
    }

    def create() {
        respond new Participation(params)
    }

    @Transactional
    def save(Participation participationInstance) {
        if (participationInstance == null) {
            notFound()
            return
        }

        if (participationInstance.hasErrors()) {
            respond participationInstance.errors, view:'create'
            return
        }

        participationInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'participation.label', default: 'Participation'), participationInstance.id])
                redirect participationInstance
            }
            '*' { respond participationInstance, [status: CREATED] }
        }
    }

    def edit(Participation participationInstance) {
        respond participationInstance
    }

    @Transactional
    def update(Participation participationInstance) {
        if (participationInstance == null) {
            notFound()
            return
        }

        if (participationInstance.hasErrors()) {
            respond participationInstance.errors, view:'edit'
            return
        }

        participationInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Participation.label', default: 'Participation'), participationInstance.id])
                redirect participationInstance
            }
            '*'{ respond participationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Participation participationInstance) {

        if (participationInstance == null) {
            notFound()
            return
        }

        participationInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Participation.label', default: 'Participation'), participationInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'participation.label', default: 'Participation'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
