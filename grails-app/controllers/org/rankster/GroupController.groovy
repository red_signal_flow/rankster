package org.rankster


import org.springframework.security.access.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.rankster.Membership

@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
@Transactional(readOnly = true)
class GroupController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Group.list(params), model:[groupInstanceCount: Group.count()]
    }

    def show(Group groupInstance) {
        def userLoggedIn = springSecurityService.isLoggedIn()
        def userJoined = false
        def currentUser = springSecurityService.getCurrentUser()

        if (userLoggedIn) {
            userJoined = groupInstance.isMember(springSecurityService.getCurrentUser())
        }

        respond groupInstance, model: [userLoggedIn: userLoggedIn, userJoined: userJoined, currentUser: currentUser]
    }

    def create() {
        respond new Group(params)
    }

    @Transactional
    @Secured(['IS_AUTHENTICATED_FULLY'])
    def joinGroup() {
        def user = springSecurityService.getCurrentUser()
        def group = Group.findById(params['id'])

        Membership.link(user, group)

        flash.message = message(message: "You joined in group")
        redirect(uri: request.getHeader('referer'))
    }

    @Transactional
    @Secured(['IS_AUTHENTICATED_FULLY'])
    def leaveGroup() {
        def user = springSecurityService.getCurrentUser()
        def group = Group.findById(params['id'])

        Membership.unlink(user, group)

        flash.message = message(message: "You leave of this group")
        redirect(uri: request.getHeader('referer'))
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def currentUserCreate() {
        respond new Group(params)
    }

    def userGroups(Integer max) {
        if (!params.containsKey('id')) {
            params << [id: springSecurityService.getCurrentUser().getId()]
        }
        if (params == null) {
            params = [id: springSecurityService.getCurrentUser().getId()]
        }
        def userInstance = User.findById(params['id'])
        def userLoggedIn = springSecurityService.isLoggedIn()
        def currentUser = springSecurityService.getCurrentUser()

        respond Group.findAllByOwner(User.findById(userInstance.id)), model: [groupInstanceCount: Group.count(), userInstance: userInstance, userLoggedIn: userLoggedIn, currentUser: currentUser]
    }

    @Transactional
    def userSave(Group groupInstance) {
        def userId = springSecurityService.getCurrentUser().getId()
        params << ['owner.id': userId, owner:[id: userId]]

        groupInstance = new Group(params)
        saveGroup(groupInstance, 'currentUserCreate', springSecurityService.getCurrentUser())
    }

    @Transactional
    def save(Group groupInstance) {
        saveGroup(groupInstance, 'create')
    }

    def edit(Group groupInstance) {
        respond groupInstance
    }

    @Transactional
    def update(Group groupInstance) {
        if (groupInstance == null) {
            notFound()
            return
        }

        if (groupInstance.hasErrors()) {
            respond groupInstance.errors, view:'edit'
            return
        }

        groupInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Group.label', default: 'Group'), groupInstance.id])
                redirect groupInstance
            }
            '*'{ respond groupInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Group groupInstance) {

        if (groupInstance == null) {
            notFound()
            return
        }

        groupInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Group.label', default: 'Group'), groupInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'group.label', default: 'Group'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    private void saveGroup(Group groupInstance, String backTo, User ownerUser = null) {
        if (groupInstance == null) {
            notFound()
            return
        }

        if (groupInstance.hasErrors()) {
            respond groupInstance.errors, view:backTo
            return
        }

        groupInstance.save flush:true

        if (ownerUser) {
            Membership.link(ownerUser, groupInstance)
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'group.label', default: 'Group'), groupInstance.id])
                redirect groupInstance
            }
            '*' { respond groupInstance, [status: CREATED] }
        }
    }

    @Transactional
	def uploadGroupPicture() {
        if(!params.containsKey('groupId'))
        {
            notFound()
            return
        }
        def groupInstance = Group.findById(params['groupId'])

        // verifica se o grupo e valido
        if (groupInstance == null) {
            notFound()
            return
        }
		// cria o diretorio dos pictures
        String pictureDir = getPictureDir(groupInstance.id)

		// pega a image e salva
		def f = request.getFile('picture')
		if (f.empty) {
			flash.message = 'file cannot be empty'
			return
		}

		String file_name = f.getOriginalFilename()
		String file_path = pictureDir+"/"+file_name
		groupInstance.picture = "group/"+groupInstance.id+"/"+file_name
        groupInstance.save()
		// cria o arquivo
		f.transferTo(new File(file_path))
        redirect uri: "/group/userGroups"
	}

	def getPictureDir(Long id){
		def webrootDir = servletContext.getRealPath("/") // app directory path
		String path = webrootDir+"images/group/"+id
		def directory = new File(path)
		if(!directory.exists()){
			directory.mkdirs()
		}
		return path
	}
}
