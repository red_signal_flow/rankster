package org.rankster

class Rank {

    String title
    String reward
    User publisher
    Group parent
    Date startDate
    Date endDate

    static belongsTo = [User, Group]
    static hasMany = [participations: Participation]

    static constraints = {
    }
	String toString(){
		return title
	}
}
