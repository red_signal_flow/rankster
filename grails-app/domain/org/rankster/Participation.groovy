package org.rankster

class Participation {

    User user
    Rank rank
    Integer votes

    static Membership link(user, rank) {
        def p = Membership.findByUserAndRank(user, rank)
        if (!p)
        {
            p = new Participation(user: user, rank: rank)
            p.save()
        }
        return p
    }

    static void unlink(user, rank) {
        def p = Participation.findByUserAndRank(user, rank)
        if (p)
        {
            user?.removeFromParticipations(p)
            rank?.removeFromParticipations(p)
            p.delete()
        }
    }

    static constraints = {
    }
}
