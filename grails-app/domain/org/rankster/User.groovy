package org.rankster

class User {

	transient springSecurityService

	String avatar
	String username
	String password
	String email
	String realName
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static hasMany = [groups: Group, memberships: Membership, ranks: Rank, participations: Participation]

    static mappedBy = [groups:'owner', ranks:'publisher']

	static transients = ['springSecurityService']

	static constraints = {
		username     blank: false, unique: true
		password     blank: false
		ranks        nullable: true
		groups       nullable: true
        avatar       nullable:true
	}

	static mapping = {
		table '_user'
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}
    
    String toString(){
        return username
    }    


}
