package org.rankster

class Membership {

    User user
    Group group

    static Membership link(user, group) {
        def m = Membership.findByUserAndGroup(user, group)
        if (!m)
        {
            m = new Membership(user: user, group: group)
            m.save()
        }
        return m
    }

    static void unlink(user, group) {
        def m = Membership.findByUserAndGroup(user, group)
        if (m)
        {

            user?.removeFromMemberships(m)
            group?.removeFromMemberships(m)
            m.delete()
        }
    }

    static constraints = {
    }
	String toString(){
		return "Group = "+group+"User = "+user
	}
}
