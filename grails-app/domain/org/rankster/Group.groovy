package org.rankster

import org.rankster.Membership

class Group {

    String name
    String description
    String picture
    User owner

    static belongsTo = User
    static hasMany = [ranks: Rank, memberships: Membership]
    static mappedBy = [ranks:'parent']


    static constraints = {
  		ranks nullable: true
        picture nullable: true
    }

    static mapping = {
      table '_group'
    }

    Boolean isMember(User user) {
        !Membership.findAllByGroupAndUser(Group.findById(id), user).isEmpty()
    }

    String toString(){
        return name
    }
}
