import org.rankster.*

class BootStrap {

    def init = { servletContext ->
      if(!User.count()){
        def userRole = new Role(authority: 'ROLE_ADMIN').save()

        def user = new User(username: 'adminuser', realName:'Admin', email:'asdad@asdas.com', password:'admin', enable: true).save()
        UserRole.create(user, userRole, true)
      }
    }
    def destroy = {
    }
}
