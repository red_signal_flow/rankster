
<%@ page import="org.rankster.Rank" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'rank.label', default: 'Rank')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-rank" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-rank" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="endDate" title="${message(code: 'rank.endDate.label', default: 'End Date')}" />
					
						<th><g:message code="rank.parent.label" default="Parent" /></th>
					
						<th><g:message code="rank.publisher.label" default="Publisher" /></th>
					
						<g:sortableColumn property="reward" title="${message(code: 'rank.reward.label', default: 'Reward')}" />
					
						<g:sortableColumn property="startDate" title="${message(code: 'rank.startDate.label', default: 'Start Date')}" />
					
						<g:sortableColumn property="title" title="${message(code: 'rank.title.label', default: 'Title')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${rankInstanceList}" status="i" var="rankInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${rankInstance.id}">${fieldValue(bean: rankInstance, field: "endDate")}</g:link></td>
					
						<td>${fieldValue(bean: rankInstance, field: "parent")}</td>
					
						<td>${fieldValue(bean: rankInstance, field: "publisher")}</td>
					
						<td>${fieldValue(bean: rankInstance, field: "reward")}</td>
					
						<td><g:formatDate date="${rankInstance.startDate}" /></td>
					
						<td>${fieldValue(bean: rankInstance, field: "title")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${rankInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
