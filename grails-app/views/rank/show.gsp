
<%@ page import="org.rankster.Rank" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'rank.label', default: 'Rank')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-rank" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-rank" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list rank">

				<g:if test="${rankInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="rank.endDate.label" default="End Date" /></span>

						<span class="property-value" aria-labelledby="endDate-label"><g:formatDate date="${rankInstance?.endDate}" /></span>

				</li>
				</g:if>

				<g:if test="${rankInstance?.parent}">
				<li class="fieldcontain">
					<span id="parent-label" class="property-label"><g:message code="rank.parent.label" default="Parent" /></span>

						<span class="property-value" aria-labelledby="parent-label"><g:link controller="group" action="show" id="${rankInstance?.parent?.id}">${rankInstance?.parent?.encodeAsHTML()}</g:link></span>

				</li>
				</g:if>

				<g:if test="${rankInstance?.participations}">
				<li class="fieldcontain">
					<span id="participations-label" class="property-label"><g:message code="rank.participations.label" default="Participations" /></span>

						<g:each in="${rankInstance.participations}" var="p">
						<span class="property-value" aria-labelledby="participations-label"><g:link controller="participation" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>

				</li>
				</g:if>

				<g:if test="${rankInstance?.publisher}">
				<li class="fieldcontain">
					<span id="publisher-label" class="property-label"><g:message code="rank.publisher.label" default="Publisher" /></span>

						<span class="property-value" aria-labelledby="publisher-label"><g:link controller="user" action="show" id="${rankInstance?.publisher?.id}">${rankInstance?.publisher?.encodeAsHTML()}</g:link></span>

				</li>
				</g:if>

				<g:if test="${rankInstance?.reward}">
				<li class="fieldcontain">
					<span id="reward-label" class="property-label"><g:message code="rank.reward.label" default="Reward" /></span>

						<span class="property-value" aria-labelledby="reward-label"><g:fieldValue bean="${rankInstance}" field="reward"/></span>

				</li>
				</g:if>

				<g:if test="${rankInstance?.startDate}">
				<li class="fieldcontain">
					<span id="startDate-label" class="property-label"><g:message code="rank.startDate.label" default="Start Date" /></span>

						<span class="property-value" aria-labelledby="startDate-label"><g:formatDate date="${rankInstance?.startDate}" /></span>

				</li>
				</g:if>

				<g:if test="${rankInstance?.title}">
				<li class="fieldcontain">
					<span id="title-label" class="property-label"><g:message code="rank.title.label" default="Title" /></span>

						<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${rankInstance}" field="title"/></span>

				</li>
				</g:if>

				<div class="row">
					<div class="col-md-4">
						Participations	:
				<g:if test="${rankInstance?.participations}">
						<ul class="list-group" >
						<g:each in="${rankInstance.participations}" status="i" var="p">
							<li class="list-group-item" > User: <g:link controller="participation" action="show" id="${p.id}">${p?.user?.encodeAsHTML()}</g:link> Votes:  ${p.votes}</li>
							<g:link action="upVote" resource="${p}"><g:message code="default.button.upvote.label" default="UpVote" /></g:link>
							<g:link action="downVote" resource="${p}"><g:message code="default.button.downvote.label" default="DownVote" /></g:link>
						</g:each>
						</ul>
				</g:if>
					</div>
					<div class="col-md-4">
					</div>
				</div>

			</ol>
			<g:form url="[resource:rankInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${rankInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
