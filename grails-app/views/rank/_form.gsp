<%@ page import="org.rankster.Rank" %>



<div class="fieldcontain ${hasErrors(bean: rankInstance, field: 'endDate', 'error')} required">
	<label for="endDate">
		<g:message code="rank.endDate.label" default="End Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="endDate" precision="day"  value="${rankInstance?.endDate}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: rankInstance, field: 'parent', 'error')} required">
	<label for="parent">
		<g:message code="rank.parent.label" default="Parent" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="parent" name="parent.id" from="${org.rankster.Group.list()}" optionKey="id" required="" value="${rankInstance?.parent?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rankInstance, field: 'participations', 'error')} ">
	<label for="participations">
		<g:message code="rank.participations.label" default="Participations" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${rankInstance?.participations?}" var="p">
    <li><g:link controller="participation" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="participation" action="create" params="['rank.id': rankInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'participation.label', default: 'Participation')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: rankInstance, field: 'publisher', 'error')} required">
	<label for="publisher">
		<g:message code="rank.publisher.label" default="Publisher" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="publisher" name="publisher.id" from="${org.rankster.User.list()}" optionKey="id" required="" value="${rankInstance?.publisher?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rankInstance, field: 'reward', 'error')} required">
	<label for="reward">
		<g:message code="rank.reward.label" default="Reward" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="reward" required="" value="${rankInstance?.reward}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rankInstance, field: 'startDate', 'error')} required">
	<label for="startDate">
		<g:message code="rank.startDate.label" default="Start Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="startDate" precision="day"  value="${rankInstance?.startDate}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: rankInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="rank.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${rankInstance?.title}"/>

</div>

