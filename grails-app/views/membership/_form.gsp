<%@ page import="org.rankster.Membership" %>



<div class="fieldcontain ${hasErrors(bean: membershipInstance, field: 'group', 'error')} required">
	<label for="group">
		<g:message code="membership.group.label" default="Group" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="group" name="group.id" from="${org.rankster.Group.list()}" optionKey="id" required="" value="${membershipInstance?.group?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: membershipInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="membership.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="user" name="user.id" from="${org.rankster.User.list()}" optionKey="id" required="" value="${membershipInstance?.user?.id}" class="many-to-one"/>

</div>

