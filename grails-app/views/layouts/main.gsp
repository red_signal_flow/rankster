<!DOCTYPE html>
<html>
<head>
    <title><g:layoutTitle default="Grails"/></title>
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>
    <g:layoutHead/>
</head>
<body>

    <div class="container">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                             <button type="button" class="navbar-toggle"
                                     data-toggle="collapse"
                                     data-target="#bs-example-navbar-collapse-1">
                                     <span class="sr-only">Toggle navigation</span>
                                     <span class="icon-bar"></span>
                                     <span class="icon-bar"></span>
                                     <span class="icon-bar"></span>
                             </button>
                             <a class="navbar-brand" href="/">
                                Rankster
                             </a>
                        </div>

                        <div class="collapse navbar-collapse" id="navbar">
							<ul class="nav navbar-nav navbar-left">
								<li><g:link   uri='/user'>Users</g:link></li>
								<li><g:link   uri='/group'>Groups</g:link></li>
								<li><g:link   uri='/rank'>Ranks</g:link></li>
							</ul>

                            <ul class="nav navbar-nav navbar-right">
                                <sec:ifNotLoggedIn>
                                    <li>
                                        <g:link controller='login' action='auth'>
                                            <span class="glyphicon glyphicon-log-in"></span>&nbsp;Login
                                        </g:link>
                                    </li>
                                    <li>
                                        <g:link controller='user' action='register'>
                                            <span class="glyphicon glyphicon-plus"></span>&nbsp;Sign up
                                        </g:link>
                                    </li>
                                </sec:ifNotLoggedIn>

                                <sec:ifLoggedIn>
                                    <li class="dropdown">
                                       <a href="#" class="dropdown-toggle"
                                                   data-toggle="dropdown">
                                                        <sec:username/>
                                       <strong class="caret">
                                       </strong>
                                       </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <g:link controller='user' action='myProfile'>
                                                    <span class="glyphicon glyphicon-user"></span>&nbsp;Profile
                                                </g:link>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <g:link controller='group' action='userGroups'>
                                                    <span class="glyphicon glyphicon-th-list"></span>&nbsp;My Groups
                                                </g:link>
                                                <g:link controller='group' action='currentUserCreate'>
                                                    <span class="glyphicon glyphicon-plus"></span>&nbsp;New Group
                                                </g:link>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <g:link controller='logout' action=''>
                                                    <span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout
                                                </g:link>
                                            </li>
                                        </ul>
                                    </li>
                                </sec:ifLoggedIn>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>


    <!-- container-->
    <div class="container"  style="margin-top: 100px"   >
        <g:layoutBody/>
    </div>
</body>
</html>
