<%@ page import="org.rankster.Participation" %>



<div class="fieldcontain ${hasErrors(bean: participationInstance, field: 'rank', 'error')} required">
	<label for="rank">
		<g:message code="participation.rank.label" default="Rank" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="rank" name="rank.id" from="${org.rankster.Rank.list()}" optionKey="id" required="" value="${participationInstance?.rank?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: participationInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="participation.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="user" name="user.id" from="${org.rankster.User.list()}" optionKey="id" required="" value="${participationInstance?.user?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: participationInstance, field: 'votes', 'error')} required">
	<label for="votes">
		<g:message code="participation.votes.label" default="Votes" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="votes" type="number" value="${participationInstance.votes}" required=""/>

</div>

