<%@ page import="org.rankster.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
        <div class='container'>

            <g:if test="${userInstance?.avatar}">
                <!-- avatar image displayed   -->
                <g:img dir="images" file="${userInstance.avatar} " width="200" height="100"class="img-thumbnail"/>
            </g:if>
            <g:else>
                <!-- default image displayed   -->
                <g:img dir="images" file="user/default/avatar.png" width="200" height="100"class="img-thumbnail"/>
            </g:else>

            <hr>

            <g:if test="${userInstance?.realName}">
                <span id="realName-label" class="property-label"><g:message code="user.realName.label" default="Real Name" />:</span>
                <span class="property-value" aria-labelledby="realName-label"><g:fieldValue bean="${userInstance}" field="realName"/></span>
            <hr>
            </g:if>
            <g:if test="${userInstance?.username}">
                <span id="username-label" class="property-label"><g:message code="user.username.label" default="Username" />:</span>
                <span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${userInstance}" field="username"/></span>
            <hr>
            </g:if>
            <g:if test="${userInstance?.email}">
                <span id="email-label" class="property-label"><g:message code="user.email.label" default="Email" />:</span>
                <span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${userInstance}" field="email"/></span>
            <hr>
            </g:if>
            <g:if test="${userInstance?.groups}">
                        %{-- <span id="groups-label" class="property-label"><g:message code="user.groups.label" default="Groups" /></span> --}%
                        <g:link controller='group' action='userGroups' params="${[id: userInstance.id]}" class='btn btn-primary'>
                            <span class="glyphicon glyphicon-th-list"></span>&nbsp;Groups
                        </g:link>
            <hr>
            </g:if>
        </div>
	</body>
</html>
