<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="create-user" class="content scaffold-create" role="main">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${userInstance}">
            <ul class="errors" role="alert">
                <g:eachError bean="${userInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form url="[resource:userInstance, action:'registerNewUser']" >
                <fieldset class="form">
                    <div class="fieldcontain ${hasErrors(bean: userInstance, field: 'realName', 'error')} required">
                        <label for="realName">
                            <g:message code="user.realName.label" default="Real Name" />
                            <span class="required-indicator">*</span>
                        </label>
                        <g:textField name="realName" required="" value="${userInstance?.realName}"/>
                    </div>
                    <div class="fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
                        <label for="username">
                            <g:message code="user.username.label" default="Username" />
                            <span class="required-indicator">*</span>
                        </label>
                        <g:textField name="username" required="" value="${userInstance?.username}"/>
                    </div>
                    <div class="fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'error')} required">
                        <label for="password">
                            <g:message code="user.password.label" default="Password" />
                            <span class="required-indicator">*</span>
                        </label>
                        <g:passwordField name="password" required="" value="${userInstance?.password}"/>
                    </div>
                    <div class="fieldcontain ${hasErrors(bean: userInstance, field: 'email', 'error')} required">
                        <label for="email">
                            <g:message code="user.email.label" default="Email" />
                            <span class="required-indicator">*</span>
                        </label>
                        <g:textField name="email" required="" value="${userInstance?.email}"/>
                    </div>

                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="register" class="save" value="${message(code: 'default.button.register.label', default: 'Register')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
