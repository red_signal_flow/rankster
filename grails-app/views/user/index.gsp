<%@ page import="org.rankster.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title>  Users   </title>
	</head>
	<body>
		<div id="list-user" class="content scaffold-list" role="main">
    
            <h1> Users </h1>
            <hr>
                    <div class="row">
				<g:each in="${userInstanceList}" status="i" var="userInstance">

                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <g:if test="${userInstance?.avatar}">
                                <!-- avatar image displayed   -->
                                <g:img dir="images" file="${userInstance.avatar} " width="200" height="100"class="img-thumbnail"/>
                            </g:if>
                            <g:else>
                                <!-- default image displayed   -->
                                <g:img dir="images" file="user/default/avatar.png" width="200" height="100"class="img-thumbnail" style="margin-bottom:40px;"/>
                            </g:else>
                          <div class="caption text-center">
						    <g:link  action="show" id="${userInstance.id}" >
                                    ${fieldValue(bean: userInstance, 
                                    field: "username")}
                            </g:link>
                          </div>
                        </div>
                      </div>
				</g:each>
                    </div>
			<div class="pagination">
				<g:paginate total="${userInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
