
<%@ page import="org.rankster.Group" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
		<title> Show Group</title>
	</head>
	<body>
            <div class='message' >
			<g:if test="${flash.message}">
			    ${flash.message}
			</g:if>
            </div>
                    <div class="pull-right">
                        <g:if test="${userLoggedIn}">
                            <g:if test="${!userJoined}">
                                <g:link controller='group' action='joinGroup' params="${[id: groupInstance.id]}" class='btn btn-primary'>
                                    <span class="glyphicon glyphicon-log-in"></span>&nbsp;Join
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link controller='group' action='leaveGroup' params="${[id: groupInstance.id]}" class='btn btn-danger'>
                                    <span class="glyphicon glyphicon-log-in"></span>&nbsp;Leave
                                </g:link>
                            </g:else>
                        </g:if>
                    </div>
                    <div>
                    <g:if test="${groupInstance?.picture}">
                        <!-- avatar image displayed   -->
                        <g:img dir="images" file="${groupInstance.picture} " width="200" height="100"class="img-thumbnail"/>
                    </g:if>
                    <g:else>
                        <!-- default image displayed   -->
                        <g:img dir="images" file="group/default/group_picture.jpg" width="200" height="100"class="img-thumbnail"/>
                    </g:else>
                    </div>
                    <br>
                    <hr>

                    <g:if test="${groupInstance?.name}">
                        <span id="name-label" >
                            <g:message code="group.name.label" default="Name" />
                        </span>
                        <span class="property-value" aria-labelledby="name-label">
                            <g:fieldValue bean="${groupInstance}" field="name"/>
                        </span>
                    </g:if>
                    <hr>
                    <g:if test="${groupInstance?.description}">
                        <span id="description-label" >
                            <g:message code="group.description.label" default="Description" />
                        </span>
                        <span class="property-value" aria-labelledby="description-label">
                            <g:fieldValue bean="${groupInstance}" field="description"/>
                        </span>
                    </g:if>
                    <hr>

                    <g:if test="${groupInstance?.owner}">
                        <span id="owner-label">
                            <g:message code="group.owner.label" default="Owner" />
                        </span>
                        <span class="property-value" aria-labelledby="owner-label">
                            <g:link controller="user" action="show" id="${groupInstance?.owner?.id}">
                                ${groupInstance?.owner?.encodeAsHTML()}
                            </g:link>
                        </span>
                    </g:if>
                    <hr>
                    <g:link controller="group" action="edit" id="${groupInstance?.id}">
                        Edit Group
                    </g:link>
                    <hr>
                    <g:link controller="rank" action="create" params="['group.id': groupInstance?.id]">
                        Create Rank
                    </g:link>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            Ranks	:
                    <g:if test="${groupInstance?.ranks}">
                            <ul class="list-group" >
                            <g:each in="${groupInstance.ranks}" status="i" var="rank">
                                <li class="list-group-item" >
                                     <g:link controller="rank" action="show" id="${rank?.id}">
                                        ${rank?.encodeAsHTML()}
                                    </g:link>
                                </li>
                            </g:each>
                            </ul>
                    </g:if>
                        </div>
                        <div class="col-md-4">
                            Members :
                    <g:if test="${groupInstance?.memberships}">
                            <ul class="list-group" >
                            <g:each in="${groupInstance.memberships}" status="i" var="member">
                                <li class="list-group-item" >
                                    <g:link controller="user" action="show" id="${member?.user?.id}">
                                        ${member?.user?.encodeAsHTML()}
                                    </g:link>
                                </li>
                            </g:each>
                            </ul>
                    </g:if>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <br>
	</body>
</html>
