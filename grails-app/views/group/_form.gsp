<%@ page import="org.rankster.Group" %>



<div class="fieldcontain ${hasErrors(bean: groupInstance, field: 'ranks', 'error')} ">
	<label for="ranks">
		<g:message code="group.ranks.label" default="Ranks" />

	</label>

<ul class="one-to-many">
<g:each in="${groupInstance?.ranks?}" var="r">
    <li><g:link controller="rank" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="rank" action="create" params="['group.id': groupInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'rank.label', default: 'Rank')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: groupInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="group.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${groupInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: groupInstance, field: 'memberships', 'error')} ">
	<label for="memberships">
		<g:message code="group.memberships.label" default="Memberships" />

	</label>

<ul class="one-to-many">
<g:each in="${groupInstance?.memberships?}" var="m">
    <li><g:link controller="membership" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="membership" action="create" params="['group.id': groupInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'membership.label', default: 'Membership')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: groupInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="group.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${groupInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: groupInstance, field: 'owner', 'error')} required">
	<label for="owner">
		<g:message code="group.owner.label" default="Owner" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="owner" name="owner.id" from="${org.rankster.User.list()}" optionKey="id" required="" value="${groupInstance?.owner?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: groupInstance, field: 'picture', 'error')} required">
	<label for="picture">
		<g:message code="group.picture.label" default="Picture" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="picture" required="" value="${groupInstance?.picture}"/>

</div>

