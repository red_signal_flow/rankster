
<%@ page import="org.rankster.Group" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
        <title> My Groups </title>
    </head>
    <body>
        <div id="list-group" class="content scaffold-list" role="main">
            <h1><g:message message="${userInstance.username} Group's" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
                <g:each in="${groupInstanceList}" status="i" var="groupInstance">
                    <div class="float-left">
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <g:if test="${groupInstance?.picture}">
                                <!-- avatar image displayed   -->
                                <g:img dir="images" file="${groupInstance.picture} " width="200" height="100"class="img-thumbnail"/>
                            </g:if>
                            <g:else>
                                <!-- default image displayed   -->
                                <g:img dir="images" file="group/default/group_picture.jpg" width="200" height="100"class="img-thumbnail" style="margin-bottom:50px;"/>
                            </g:else>
                            <div class="caption text-center">
                        		<g:link action="show" id="${groupInstance.id}">
											${fieldValue(bean: groupInstance, field: "name")}
								</g:link>
                                <br />
                                <g:if test="${userLoggedIn}">
                                    <g:if test="${!groupInstance.isMember(currentUser)}">
                                        <g:link controller='group' action='joinGroup' params="${[id: groupInstance.id]}" class='btn btn-primary'>
                                            <span class="glyphicon glyphicon-log-in"></span>&nbsp;Join
                                        </g:link>
                                    </g:if>
                                    <g:else>
                                        <g:link controller='group' action='leaveGroup' params="${[id: groupInstance.id]}" class='btn btn-danger'>
                                            <span class="glyphicon glyphicon-log-in"></span>&nbsp;Leave
                                        </g:link>
                                    </g:else>
                                </g:if>
                            </div>
						</div>
					  </div>
				   </div>
                </g:each>
            <div class="pagination">
                <g:paginate total="${groupInstanceCount ?: 0}" />
            </div>
        </div>
    </body>
</html>
