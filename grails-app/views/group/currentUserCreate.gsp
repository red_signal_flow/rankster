<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
        <title>New Group</title>
    </head>
    <body>
        <div id="create-group" class="content scaffold-create" role="main">
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${groupInstance}">
            <ul class="errors" role="alert">
                <g:eachError bean="${groupInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <div class="container">
            <g:form class="form-horizontal" url="[resource:groupInstance, action:'userSave']" >
                <fieldset>

                    <legend>New Group</legend>

                    <div class="control-group">
                        <label class="control-label"  for="name">
                            <g:message code="group.name.label" default="Name" />
                            <span class="required-indicator">*</span>:
                        </label>
                            <g:textField name="name" class="input-xxlarge" required="" value="${groupInstance?.name}"/>
                    </div>
					<hr>


                    <div class="control-group">
                        <label class="control-label"    for="description">
                            <g:message code="group.description.label" default="Description" />
                            <span class="required-indicator">*</span>:
                        </label>
                        <g:textArea name="description" required="" value="${groupInstance?.description}"/>
                    </div>
					<hr>

                    <g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                </fieldset>
            </g:form>
            </div>
        </div>
    </body>
</html>
