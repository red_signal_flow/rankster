
<%@ page import="org.rankster.Group" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
		<title> Groups  </title>
	</head>
	<body>
			<h1>Groups</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>

            <g:each in="${groupInstanceList}" status="i" var="groupInstance">
                <div class="float-left">
                  <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="text-center">
                            <g:link action="show" id="${groupInstance.id}">
                            ${fieldValue(bean: groupInstance, field: "name")}
                            </g:link>
                        </div>
                        <g:if test="${groupInstance?.picture}">
                            <!-- avatar image displayed   -->
                            <g:img dir="images" file="${groupInstance.picture} " width="200" height="100"class="img-thumbnail"/>
                        </g:if>
                        <g:else>
                            <!-- default image displayed   -->
                            <g:img dir="images" file="group/default/group_picture.jpg" width="200" height="100"class="img-thumbnail" style="margin-bottom:50px;"/>
                        </g:else>
                      <div class="caption text-center">
                            ${fieldValue(bean: groupInstance, field: "description")}
                        <hr>
                        Owner: 
                            ${fieldValue(bean: groupInstance, field: "owner")}
                        <hr>
                        </div>
                    </div>
                </div>

			</g:each>
			<div class="pagination">
				<g:paginate total="${groupInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
