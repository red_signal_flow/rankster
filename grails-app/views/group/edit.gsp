<%@ page import="org.rankster.Group" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
		<title> Edit Group </title>
	</head>
	<body>
		<div id="edit-group" class="content scaffold-edit" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${groupInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${groupInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>


		<g:if test="${groupInstance?.picture}">
			<!-- avatar image displayed   -->
			<g:img dir="images" file="${groupInstance.picture} " width="200" height="100"class="img-thumbnail"/>
		</g:if>
		<g:else>
			<!-- default image displayed   -->
			<g:img dir="images" file="group/default/group_picture.jpg" width="200" height="100"class="img-thumbnail"/>
		</g:else>

		<g:uploadForm action='uploadGroupPicture' >
			<input type='hidden' name='groupId' value="${groupInstance.id}">
			<input type="file" name="picture" />
			<input type="submit" value="Change" />
		</g:uploadForm>	
		<hr>

			<g:form url="[resource:groupInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${groupInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
