Rankster
===========

Dependencies
=========

Services:
   
- grails - 2.4.4
- postgresql - 9.4

Plugins:

- Searchable            
- Spring Security Core  version - 2.0.0 
- asset-pipeline        version - 2.1.5
- twitter-bootstrap     version - 3.3.4

How to install
=========

Create User:
-----------
 $ createuser -dlrP -U postgres rankster

Create Database Development:
----------
  $ psql postgres -U rankster -c "CREATE DATABASE rankster WITH OWNER rankster"

Create Database Production:
--------------
  $ psql postgres -U rankster -c 
    "CREATE DATABASE rankster_production WITH OWNER rankster "








